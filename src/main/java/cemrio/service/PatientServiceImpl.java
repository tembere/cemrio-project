
package cemrio.service;

import java.time.LocalDate;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import cemrio.entity.Patient;
import cemrio.entity.User;
import cemrio.exception.PatientNotFoundException;
import cemrio.repository.IPatientRepo;
import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class PatientServiceImpl implements IPatientService {

	@Autowired
	private IPatientRepo patientRepo;

	@Autowired
	private AuthorizationService authorizationService;

	@Override
	@Transactional
	public Patient add(Patient patient) {
		User user = authorizationService.getUser();
		patient.setUser(user);
		Patient p = patientRepo.save(patient);
		log.info("Patient " + p.getNom() + " crée" + " par " + user.getUsername());
		return patient;
	}

	@Override
	public Patient get(Long id) {
		return patientRepo.findById(id).orElseThrow(() -> new PatientNotFoundException(id));
	}

	@Override
	public Page<Patient> getAll() {
		return patientRepo.findAll(PageRequest.of(0, 30, Sort.by(Sort.Direction.ASC, "nom")));
	}

	@Override
	@Transactional
	public void delete(Long id) {
		log.info("patient de id " + id + " supprimé par l'utilisateur " + authorizationService.getUser());
		patientRepo.deleteById(id);
	}

	@Override
	public List<Patient> getByNom(String nom) {
		List<Patient> patients = patientRepo.findByNom(nom);
		if (patients.isEmpty()) {
			throw new PatientNotFoundException(nom);
		}
		return patients;
	}

	@Override
	@Transactional
	public Patient editName(Long id, String name) {
		Patient patient = get(id);
		patient.setNom(name);
		User user = authorizationService.getUser();
		patient.setUser(user);
		return patientRepo.save(patient);
	}

	@Override
	@Transactional
	public Patient editSurname(Long id, @Valid String prenom) {
		Patient patient = get(id);
		patient.setPrenom(prenom);
		User user = authorizationService.getUser();
		patient.setUser(user);
		return patientRepo.save(patient);
	}

	@Override
	@Transactional
	public Patient editProfession(Long id, @Valid String profession) {
		Patient patient = get(id);
		patient.setProfession(profession);
		User user = authorizationService.getUser();
		patient.setUser(user);
		return patientRepo.save(patient);
	}

	@Override
	@Transactional
	public Patient editAge(Long id, @Valid double age) {
		Patient patient = get(id);
		patient.setAge(age);
		User user = authorizationService.getUser();
		patient.setUser(user);
		return patientRepo.save(patient);
	}

	@Override
	@Transactional
	public Patient editSex(Long id, @Valid char sexe) {
		Patient patient = get(id);
		patient.setSexe(sexe);
		User user = authorizationService.getUser();
		patient.setUser(user);
		return patientRepo.save(patient);
	}

	@Override
	@Transactional
	public Patient editTelephone(Long id, @Valid String telephone) {
		Patient patient = get(id);
		patient.setTelephone(telephone);
		User user = authorizationService.getUser();
		patient.setUser(user);
		return patientRepo.save(patient);
	}

	@Override
	@Transactional
	public Patient editDdr(Long id, @Valid LocalDate ddr) {
		Patient patient = get(id);
		patient.setDdr(ddr);
		User user = authorizationService.getUser();
		patient.setUser(user);
		return patientRepo.save(patient);
	}

	@Override
	@Transactional
	public Patient edit(Long id, Patient p) {
		Patient patient = get(id);
		patient.setNom(p.getNom());
		patient.setPrenom(p.getPrenom());
		patient.setProfession(p.getProfession());
		patient.setSexe(p.getSexe());
		patient.setTelephone(p.getTelephone());
		patient.setDdr(p.getDdr());
		patient.setAge(p.getAge());
		User user = authorizationService.getUser();
		patient.setUser(user);
		return patientRepo.save(patient);
	}
}
