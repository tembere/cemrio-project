package cemrio.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import cemrio.entity.ERole;
import cemrio.entity.Privilege;
import cemrio.entity.Role;

public interface IRoleRepo extends JpaRepository<Role, Long> {
	Optional<Role> findByNom(ERole name);

	Role findTopByPrivilegesOrderByIdDesc(Privilege privilege);
}
