package cemrio.controller;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.time.LocalDate;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import javax.validation.Valid;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cemrio.dto.patient.PatientDtoExportReq;
import cemrio.dto.patient.PatientDtoExportRes;
import cemrio.dto.patient.PatientDtoReq;
import cemrio.dto.patientExamPresc.PatientExamPrescDto;
import cemrio.entity.Patient;
import cemrio.entity.PatientExamPresc;
import cemrio.service.AuthorizationService;
import cemrio.service.IPatientExamPrescService;
import cemrio.service.IPatientService;
import cemrio.util.ExcelBuilder;
import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.info.Info;
import io.swagger.v3.oas.annotations.media.ArraySchema;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.tags.Tag;

@OpenAPIDefinition(info = @Info(title = "API CEMRIO", description = "Documentation de l'API", version = "1.0"))
@RestController
@RequestMapping("/patients")
@Tag(name = "patient", description = "A propos du patient")
public class PatientRest {

	@Autowired
	private IPatientService patientService;

	@Autowired
	private IPatientExamPrescService patientExamPrescService;

	@Autowired
	private AuthorizationService authorizationService;

	@Autowired
	private ModelMapper modelMapper;

	@Operation(summary = "Ajoute un patient dans la BD", tags = "patient", responses = {
			@ApiResponse(responseCode = "200", description = "Succès de l'opération", content = @Content(mediaType = "Application/Json", array = @ArraySchema(schema = @Schema(implementation = Patient.class)))),
			@ApiResponse(responseCode = "400", description = "Erreur: Ce nom d'utilisateur est déjà utilisé/Erreur: Cet email est déjà utilisé", content = @Content(mediaType = "Application/Json")),
			@ApiResponse(responseCode = "403", description = "Forbidden : accès refusé", content = @Content(mediaType = "Application/Json")),
			@ApiResponse(responseCode = "401", description = "Full authentication is required to access this resource", content = @Content(mediaType = "Application/Json")) })
	@PostMapping
	public ResponseEntity<Patient> add(@RequestBody PatientDtoReq paDtoReq) {
		Patient p = modelMapper.map(paDtoReq, Patient.class);
		Patient patient = patientService.add(p);
		return ResponseEntity.ok(patient);
	}

	@Operation(summary = "Recupère un patient dans la BD à partir de son id", tags = "patient", responses = {
			@ApiResponse(responseCode = "200", description = "Succès de l'opération", content = @Content(mediaType = "Application/Json", array = @ArraySchema(schema = @Schema(implementation = Patient.class)))),
			@ApiResponse(responseCode = "404", description = "Le Patient n'existe pas dans la BD", content = @Content(mediaType = "Application/Json")) })
	@GetMapping("/{id:[0-9]+}")
	public ResponseEntity<Patient> get(@PathVariable Long id) {
		Patient patient = patientService.get(id);
		return ResponseEntity.ok(patient);
	}

	@Operation(summary = "Liste paginée et triée par ordre croissant de nom de tous les patients crées", tags = "patient", responses = {
			@ApiResponse(responseCode = "200", description = "Succès de l'opération", content = @Content(mediaType = "Application/Json", array = @ArraySchema(schema = @Schema(implementation = Patient.class)))) })
	@GetMapping
	public ResponseEntity<Page<Patient>> getAll() {
		Page<Patient> patients = patientService.getAll();
		return ResponseEntity.ok(patients);
	}

	@Operation(summary = "Recherche un patient dans la BD à partir de son nom", tags = "patient", responses = {
			@ApiResponse(responseCode = "200", description = "Succès de l'opération", content = @Content(mediaType = "Application/Json", array = @ArraySchema(schema = @Schema(implementation = Patient.class)))),
			@ApiResponse(responseCode = "404", description = "Le Patient non trouvé dans la BD", content = @Content(mediaType = "Application/Json")) })
	@GetMapping("/{nom:[a-z]+}")
	public ResponseEntity<List<Patient>> search(@PathVariable @Valid String nom) {
		List<Patient> patients = patientService.getByNom(nom);
		return ResponseEntity.ok(patients);
	}

	@Operation(summary = "Supprime un patient de la BD", tags = "patient", responses = {
			@ApiResponse(responseCode = "200", description = "Succès de l'opération") })
	@DeleteMapping("/{id:[0-9]+}")
	@PreAuthorize("hasRole('PRIVE2') or @authorizationService.canDelete('delete', 'Patient', #id)")
	public ResponseEntity<String> delete(@PathVariable Long id) {
		patientService.delete(id);
		return ResponseEntity.ok("Patient supprimé avec succès");
	}

	@Operation(summary = "modifie le nom d'un patient ", tags = "patient", responses = {
			@ApiResponse(responseCode = "200", description = "Succès de l'opération", content = @Content(mediaType = "Application/Json", array = @ArraySchema(schema = @Schema(implementation = Patient.class)))) })
	@PutMapping("/{id:[0-9]+}/nom")
	@PreAuthorize("hasRole('PRIVE2') or @authorizationService.canUpdatePersonalInformations('update', 'Patient', #id)")
	public ResponseEntity<Patient> editName(@PathVariable Long id, @Valid @PathVariable String nom) {
		Patient patient = patientService.editName(id, nom);
		return ResponseEntity.ok(patient);
	}

	@Operation(summary = "modifie le prenom d'un patient ", tags = "patient", responses = {
			@ApiResponse(responseCode = "200", description = "Succès de l'opération", content = @Content(mediaType = "Application/Json", array = @ArraySchema(schema = @Schema(implementation = Patient.class)))) })
	@PutMapping("/{id:[0-9]+}/prenom")
	@PreAuthorize("hasRole('PRIVE2') or @authorizationService.canUpdatePersonalInformations('update', 'Patient', #id)")
	public ResponseEntity<Patient> editSurname(@PathVariable Long id, @Valid @PathVariable String prenom) {
		Patient patient = patientService.editSurname(id, prenom);
		return ResponseEntity.ok(patient);
	}

	@Operation(summary = "modifie la profession d'un patient ", tags = "patient", responses = {
			@ApiResponse(responseCode = "200", description = "Succès de l'opération", content = @Content(mediaType = "Application/Json", array = @ArraySchema(schema = @Schema(implementation = Patient.class)))) })
	@PutMapping("/{id:[0-9]+}/profession")
	@PreAuthorize("hasRole('PRIVE2') or @authorizationService.canUpdatePersonalInformations('update', 'Patient', #id)")
	public ResponseEntity<Patient> editProfession(@PathVariable Long id, @Valid @PathVariable String profession) {
		Patient patient = patientService.editProfession(id, profession);
		return ResponseEntity.ok(patient);
	}

	@Operation(summary = "modifie l'age d'un patient ", tags = "patient", responses = {
			@ApiResponse(responseCode = "200", description = "Succès de l'opération", content = @Content(mediaType = "Application/Json", array = @ArraySchema(schema = @Schema(implementation = Patient.class)))) })
	@PutMapping("/{id:[0-9]+}/age")
	@PreAuthorize("hasRole('PRIVE2') or @authorizationService.canUpdatePersonalInformations('update', 'Patient', #id)")
	public ResponseEntity<Patient> editAge(@PathVariable Long id, @Valid @PathVariable double age) {
		Patient patient = patientService.editAge(id, age);
		return ResponseEntity.ok(patient);
	}

	@Operation(summary = "modifie le sexe d'un patient ", tags = "patient", responses = {
			@ApiResponse(responseCode = "200", description = "Succès de l'opération", content = @Content(mediaType = "Application/Json", array = @ArraySchema(schema = @Schema(implementation = Patient.class)))) })
	@PutMapping("/{id:[0-9]+}/sexe")
	@PreAuthorize("hasRole('PRIVE2') or @authorizationService.canUpdatePersonalInformations('update', 'Patient', #id)")
	public ResponseEntity<Patient> editSex(@PathVariable Long id, @Valid @PathVariable char sexe) {
		Patient patient = patientService.editSex(id, sexe);
		return ResponseEntity.ok(patient);
	}

	@Operation(summary = "modifie le telephone d'un patient ", tags = "patient", responses = {
			@ApiResponse(responseCode = "200", description = "Succès de l'opération", content = @Content(mediaType = "Application/Json", array = @ArraySchema(schema = @Schema(implementation = Patient.class)))) })
	@PutMapping("/{id:[0-9]+}/telephone")
	@PreAuthorize("hasRole('PRIVE2') or @authorizationService.canUpdatePersonalInformations('update', 'Patient', #id)")
	public ResponseEntity<Patient> editTelephone(@PathVariable Long id, @Valid @PathVariable String telephone) {
		Patient patient = patientService.editTelephone(id, telephone);
		return ResponseEntity.ok(patient);
	}

	@Operation(summary = "modifie la ddr d'un patient ", tags = "patient", responses = {
			@ApiResponse(responseCode = "200", description = "Succès de l'opération") })
	@PutMapping("/{id:[0-9]+}/ddr")
	@PreAuthorize("hasRole('PRIVE2') or @authorizationService.canUpdatePersonalInformations('update', 'Patient', #id)")
	public ResponseEntity<Patient> editDdr(@PathVariable Long id, @Valid @PathVariable LocalDate ddr) {
		Patient patient = patientService.editDdr(id, ddr);
		return ResponseEntity.ok(patient);
	}

	@Operation(summary = "modifie un patient de la BD", tags = "patient", responses = {
			@ApiResponse(responseCode = "200", description = "Succès de l'opération", content = @Content(mediaType = "Application/Json", array = @ArraySchema(schema = @Schema(implementation = Patient.class)))) })
	@PutMapping("/{id:[0-9]+}")
	@PreAuthorize("hasRole('PRIVE2') or @authorizationService.canUpdatePersonalInformations('update', 'Patient', #id)")
	public ResponseEntity<Patient> edit(@PathVariable Long id, @RequestBody @Valid PatientDtoReq patientDtoReq) {
		Patient patient = modelMapper.map(patientDtoReq, Patient.class);
		Patient p = patientService.edit(id, patient);
		return ResponseEntity.ok(p);
	}

	@Operation(summary = "Associe un examen et eventuellement un prescripteur à un patient ", tags = "patient", responses = {
			@ApiResponse(responseCode = "200", description = "Succès de l'opération", content = @Content(mediaType = "Application/Json", array = @ArraySchema(schema = @Schema(implementation = PatientExamPresc.class)))) })
	@PostMapping("/patient-examen-prescripteur")
	public ResponseEntity<Set<PatientExamPresc>> add(@RequestBody Set<PatientExamPrescDto> patientExamPrescDto) {
		Set<PatientExamPresc> p = patientExamPrescDto.stream().map(this::convertToEntity).collect(Collectors.toSet());
		Set<PatientExamPresc> patExamPresc = patientExamPrescService.add(p);
		return ResponseEntity.ok(patExamPresc);

	}

	@Operation(summary = "Recupère un patient et ses examens effectués dans la BD à partir de l'id d'enrégistrement", tags = "patient", responses = {
			@ApiResponse(responseCode = "200", description = "Succès de l'opération", content = @Content(mediaType = "Application/Json", array = @ArraySchema(schema = @Schema(implementation = PatientExamPresc.class)))),
			@ApiResponse(responseCode = "404", description = "La ligne Patient-Examen-Prescripteur n'existe pas dans la BD", content = @Content(mediaType = "Application/Json")) })
	@GetMapping("/patient-examen-prescripteur/{id:[0-9]+}")
	public ResponseEntity<PatientExamPresc> getPatientWithExamenAndPrescripteur(@PathVariable Long id) {
		PatientExamPresc patientExamPresc = patientExamPrescService.get(id);
		return ResponseEntity.ok(patientExamPresc);
	}

	@Operation(summary = "Liste paginée et triée par ordre croissant de nom de tous les patients crées avec leurs examens effectués et les prescripteurs correspondants ", tags = "patient", responses = {
			@ApiResponse(responseCode = "200", description = "Succès de l'opération", content = @Content(mediaType = "Application/Json", array = @ArraySchema(schema = @Schema(implementation = PatientExamPresc.class)))) })
	@GetMapping("/patient-examen-prescripteur")
	public ResponseEntity<Page<PatientExamPresc>> getAllPatientsWithExamensAndPrescripteurs() {
		Page<PatientExamPresc> patientExamPrescs = patientExamPrescService.getAllPatientsWithExamensAndPrescripteurs();
		return ResponseEntity.ok(patientExamPrescs);
	}

	@Operation(summary = "Supprime une ligne patient-examen-prescripteur de la BD", tags = "patient", responses = {
			@ApiResponse(responseCode = "200", description = "Succès de l'opération") })
	@DeleteMapping("/patient-examen-prescripteur/{id:[0-9]+}")
	@PreAuthorize("hasRole('PRIVE2') or @authorizationService.canDelete('delete', 'PatientExamPresc', #id)")
	public ResponseEntity<String> deletePatientWithExamenAndPrescripteur(@PathVariable Long id) {
		patientExamPrescService.delete(id);
		return ResponseEntity.ok("Patient avec son examen effectué supprimé avec succès");
	}

	@Operation(summary = "modifie les renseignements cliniques associé à l'examen d'un patient ", tags = "patient", responses = {
			@ApiResponse(responseCode = "200", description = "Succès de l'opération", content = @Content(mediaType = "Application/Json", array = @ArraySchema(schema = @Schema(implementation = PatientExamPresc.class)))) })
	@PutMapping("/patient-examen-prescripteur/{id:[0-9]+}/renseignements-cliniques")
	@PreAuthorize("hasRole('PRIVE2') or @authorizationService.canUpdateAggregateInformations('update', 'PatientExamPresc', #id)")
	public ResponseEntity<PatientExamPresc> editRenseignementsCligniques(@PathVariable Long id,
			@Valid @PathVariable String nom) {
		PatientExamPresc p = patientExamPrescService.editRenseignementsCligniques(id, nom);
		return ResponseEntity.ok(p);
	}

	@Operation(summary = "modifie le prescripteur associé à l'examen d'un patient ", tags = "patient", responses = {
			@ApiResponse(responseCode = "200", description = "Succès de l'opération", content = @Content(mediaType = "Application/Json", array = @ArraySchema(schema = @Schema(implementation = PatientExamPresc.class)))) })
	@PutMapping("/patient-examen-prescripteur/{id:[0-9]+}/prescripteur/{prescripteurId:[0-9]+}")
	@PreAuthorize("hasRole('PRIVE2') or @authorizationService.canUpdateAggregateInformations('update', 'PatientExamPresc', #id)")
	public ResponseEntity<PatientExamPresc> editPrescripteur(@PathVariable Long id,
			@PathVariable Long prescripteurId) {
		PatientExamPresc p = patientExamPrescService.editPrescripteur(id, prescripteurId);
		return ResponseEntity.ok(p);
	}

	@Operation(summary = "modifie l'examen associé d'un patient ", tags = "patient", responses = {
			@ApiResponse(responseCode = "200", description = "Succès de l'opération", content = @Content(mediaType = "Application/Json", array = @ArraySchema(schema = @Schema(implementation = PatientExamPresc.class)))) })
	@PutMapping("/patient-examen-prescripteur/{id:[0-9]+}/examen/{examenId:[0-9]+}")
	@PreAuthorize("hasRole('PRIVE2') or @authorizationService.canUpdateAggregateInformations('update', 'PatientExamPresc', #id)")
	public ResponseEntity<PatientExamPresc> editExamen(@PathVariable Long id,
			@PathVariable Long examenId) {
		PatientExamPresc p = patientExamPrescService.editExamen(id, examenId);
		return ResponseEntity.ok(p);
	}

	@Operation(summary = "modifie le patient associé à un examen ", tags = "patient", responses = {
			@ApiResponse(responseCode = "200", description = "Succès de l'opération", content = @Content(mediaType = "Application/Json", array = @ArraySchema(schema = @Schema(implementation = PatientExamPresc.class)))) })
	@PutMapping("/patient-examen-prescripteur/{id:[0-9]+}/patient/{patientId:[0-9]+}")
	@PreAuthorize("hasRole('PRIVE2') or @authorizationService.canUpdateAggregateInformations('update', 'PatientExamPresc', #id)")
	public ResponseEntity<PatientExamPresc> editPatient(@PathVariable Long id,
			@PathVariable Long patientId) {
		PatientExamPresc p = patientExamPrescService.editPatient(id, patientId);
		return ResponseEntity.ok(p);
	}

	@Operation(summary = "modifie plusieurs colonnes à la fois dans la table patientExamPresc ", tags = "patient", responses = {
			@ApiResponse(responseCode = "200", description = "Succès de l'opération", content = @Content(mediaType = "Application/Json", array = @ArraySchema(schema = @Schema(implementation = PatientExamPresc.class)))) })
	@PutMapping("/patient-examen-prescripteur/{id:[0-9]+}")
	@PreAuthorize("hasRole('PRIVE2') or @authorizationService.canUpdateAggregateInformations('update', 'PatientExamPresc', #id)")
	public ResponseEntity<PatientExamPresc> edit(@PathVariable Long id, @RequestBody PatientExamPresc p) {
		PatientExamPresc patientExamPresc = patientExamPrescService.edit(id, p);
		return ResponseEntity.ok(patientExamPresc);
	}

	private PatientExamPresc convertToEntity(PatientExamPrescDto p) {
		return modelMapper.map(p, PatientExamPresc.class);
	}

	@GetMapping("/patient-examen-prescripteur/export")
	public ResponseEntity<?> excelPatientExport(@RequestBody PatientDtoExportReq p)
			throws IOException {
		if (!authorizationService.canExtract(p)) {
			return ResponseEntity.ok("droits d'accès restreint");
		}
		List<PatientExamPresc> patients = patientExamPrescService.export(p);
		List<PatientDtoExportRes> paDtoExpRes = patients.stream().map(this::convertToDto).collect(Collectors.toList());
		ByteArrayInputStream in = ExcelBuilder.patientsToExcel(paDtoExpRes);

		HttpHeaders headers = new HttpHeaders();
		headers.add("Content-Disposition", "attachment; filename=patients.xlsx");

		return ResponseEntity.ok().headers(headers).body(new InputStreamResource(in));
	}

	private PatientDtoExportRes convertToDto(PatientExamPresc p) {
		return modelMapper.map(p, PatientDtoExportRes.class);
	}

}
