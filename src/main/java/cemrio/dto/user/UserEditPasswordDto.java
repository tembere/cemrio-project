
package cemrio.dto.user;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class UserEditPasswordDto {

	@NotNull
	@Size(min = 6, max = 40)
	private String oldPassword;

	@NotNull
	@Size(min = 6, max = 40)
	private String password;
}
