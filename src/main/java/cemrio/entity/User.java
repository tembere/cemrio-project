package cemrio.entity;

import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.hibernate.envers.Audited;
import org.hibernate.envers.NotAudited;

import com.fasterxml.jackson.annotation.JsonIgnore;

import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.media.Schema.AccessMode;
import lombok.AccessLevel;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Getter
@Setter
@EqualsAndHashCode
@NoArgsConstructor
@Audited
@Table(name = "users")
public class User {

	@Schema(description = "identifiant unique de l'utilisateur", example = "1", required = true, accessMode = AccessMode.READ_ONLY)
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Setter(AccessLevel.NONE)
	private Long id;

	@Schema(description = "nom de l'utilisateur", example = "stephane")
	@NotNull
	@Column(unique = true)
	@Size(min = 3, max = 20)
	private String username;

	@Schema(description = "email de l'utilisateur", example = "stephane@gmail.com")
	@Column(unique = true)
	@Email
	@Size(max = 50)
	private String email;

	@Schema(description = "mot de passe de l'utilisateur", example = "ebkn46Ai?")
	@NotNull
	@JsonIgnore
	private String password;

	@NotAudited
	@ManyToMany(fetch = FetchType.LAZY)
	private Set<Role> roles;

	@JsonIgnore
	@ManyToOne
	@JoinColumn(name = "user_id")
	private User userEdit;

	@ManyToMany
	private Set<Service> services;

	public User(String username, String email, Set<Service> services, String password) {
		this.username = username;
		this.email = email;
		this.password = password;
		this.services = services;
	}
}
