
package cemrio.dto.user;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class UserAuthDtoRequest {

	@NotNull
	@Size(max = 20)
	private String username;

	@NotNull
	@Size(max = 120)
	private String password;
}
