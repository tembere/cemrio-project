package cemrio.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.NOT_FOUND)
public class PrescripteurNotFoundException extends RuntimeException {
	public PrescripteurNotFoundException(String message) {
		super(message);
	}

	public PrescripteurNotFoundException(Long id) {
		super("Le prescripteur de id " + id + " n'a pas été trouvé  ");
	}
}
