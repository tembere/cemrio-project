package cemrio.controller;

import javax.validation.Valid;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import cemrio.dto.user.UserAddDto;
import cemrio.dto.user.UserEditByAdminDto;
import cemrio.dto.user.UserEditPasswordDto;
import cemrio.entity.User;
import cemrio.service.IUserService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.ArraySchema;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.tags.Tag;

@RestController
@RequestMapping("/users")
@Tag(name = "utilisateur", description = "A propos des utilisateurs")
public class UserRest {

	@Autowired
	IUserService userService;

	@Autowired
	private ModelMapper modelMapper;

	@Operation(summary = "Ajoute un utilisateur dans la BD", tags = "utilisateur", responses = {
			@ApiResponse(responseCode = "200", description = "Succès de l'opération", content = @Content(mediaType = "Application/Json", array = @ArraySchema(schema = @Schema(implementation = User.class)))),
			@ApiResponse(responseCode = "400", description = "Erreur: Ce nom d'utilisateur est déjà utilisé/Erreur: Cet email est déjà utilisé", content = @Content(mediaType = "Application/Json")),
			@ApiResponse(responseCode = "403", description = "Forbidden : accès refusé", content = @Content(mediaType = "Application/Json")),
			@ApiResponse(responseCode = "401", description = "Full authentication is required to access this resource", content = @Content(mediaType = "Application/Json")) })
	@PostMapping
	@PreAuthorize("hasRole('PRIVE2')")
	public ResponseEntity<?> add(@Valid @RequestBody UserAddDto userAddDto) {
		User user = modelMapper.map(userAddDto, User.class);
		return userService.add(user);
	}

	@Operation(summary = "Recupère un utilisateur dans la BD à partir de son id", tags = "utilisateur", responses = {
			@ApiResponse(responseCode = "200", description = "Succès de l'opération", content = @Content(mediaType = "Application/Json", array = @ArraySchema(schema = @Schema(implementation = User.class)))),
			@ApiResponse(responseCode = "404", description = "Erreur: User non trouvé dans la BD", content = @Content(mediaType = "Application/Json")),
			@ApiResponse(responseCode = "403", description = "Forbidden : aacès refusé", content = @Content(mediaType = "Application/Json")),
			@ApiResponse(responseCode = "401", description = "Full authentication is required to access this resource", content = @Content(mediaType = "Application/Json")) })
	@GetMapping("/{id:[0-9]+}")
	@PreAuthorize("hasRole('PRIVE2') or @authorizationService.canUpdatePersonalInformations('read', 'User', #id)")
	public ResponseEntity<User> get(@PathVariable Long id) {
		User user = userService.get(id);
		return ResponseEntity.ok(user);
	}

	@Operation(summary = "Liste paginée et triée par ordre croissant de nom de tous les uilisateurs", tags = "utilisateur", responses = {
			@ApiResponse(responseCode = "200", description = "Succès de l'opération", content = @Content(mediaType = "Application/Json", array = @ArraySchema(schema = @Schema(implementation = User.class)))),
			@ApiResponse(responseCode = "403", description = "Forbidden : aacès refusé", content = @Content(mediaType = "Application/Json")),
			@ApiResponse(responseCode = "401", description = "Full authentication is required to access this resource", content = @Content(mediaType = "Application/Json")) })
	@GetMapping
	@PreAuthorize("hasRole('PRIVE2')")
	public ResponseEntity<Page<User>> getAll() {
		Page<User> users = userService.getAll();
		return ResponseEntity.ok(users);
	}

	@Operation(summary = "Supprime un utilisateur de la BD", tags = "utilisateur", responses = {
			@ApiResponse(responseCode = "200", description = "Succès de l'opération") })
	@DeleteMapping("/{id:[0-9]+}")
	@PreAuthorize("hasRole('PRIVE2')")
	public ResponseEntity<String> delete(@PathVariable Long id) {
		userService.delete(id);
		return ResponseEntity.ok("User supprimé avec succès");
	}

	@Operation(summary = "modifie l'email d'un utilisateur ", tags = "utilisateur", responses = {
			@ApiResponse(responseCode = "200", description = "Succès de l'opération", content = @Content(mediaType = "Application/Json", array = @ArraySchema(schema = @Schema(implementation = User.class)))),
			@ApiResponse(responseCode = "404", description = "Erreur: User non trouvé dans la BD", content = @Content(mediaType = "Application/Json")) })
	@PutMapping("/{id:[0-9]+}/email")
	@PreAuthorize("hasRole('PRIVE2') or @authorizationService.canUpdatePersonalInformations('update', 'User', #id)")
	public ResponseEntity<User> editEmail(@PathVariable Long id, @Valid @RequestParam String email) {
		User user = userService.editEmail(id, email);
		return ResponseEntity.ok(user);
	}

	@Operation(summary = "modifie le username d'un utilisateur", tags = "utilisateur", responses = {
			@ApiResponse(responseCode = "200", description = "Succès de l'opération", content = @Content(mediaType = "Application/Json", array = @ArraySchema(schema = @Schema(implementation = User.class)))),
			@ApiResponse(responseCode = "404", description = "Erreur: User non trouvé dans la BD", content = @Content(mediaType = "Application/Json")) })
	@PutMapping("/{id:[0-9]+}/username")
	@PreAuthorize("hasRole('PRIVE2')")
	public ResponseEntity<User> editUsername(@PathVariable Long id, @Valid @RequestParam String username) {
		User user = userService.editUsername(id, username);
		return ResponseEntity.ok(user);
	}

	@Operation(summary = "modifie l'email, le username et le role d'un utilisateur", tags = "utilisateur", responses = {
			@ApiResponse(responseCode = "200", description = "Succès de l'opération", content = @Content(mediaType = "Application/Json", array = @ArraySchema(schema = @Schema(implementation = User.class)))),
			@ApiResponse(responseCode = "404", description = "Erreur: User non trouvé dans la BD", content = @Content(mediaType = "Application/Json")) })
	@PutMapping("/{id:[0-9]+}")
	@PreAuthorize("hasRole('PRIVE2')")
	public ResponseEntity<User> editByAdmin(@PathVariable Long id,
			@Valid @RequestBody UserEditByAdminDto userEditByAdminDto) {
		User u = modelMapper.map(userEditByAdminDto, User.class);
		User user = userService.editByAdmin(id, u);
		return ResponseEntity.ok(user);
	}

	@Operation(summary = "ajoute un service à la liste de services auxquels appartient un utilisateur", tags = "utilisateur", responses = {
			@ApiResponse(responseCode = "200", description = "Succès de l'opération", content = @Content(mediaType = "Application/Json", array = @ArraySchema(schema = @Schema(implementation = User.class)))),
			@ApiResponse(responseCode = "404", description = "Erreur: User non trouvé dans la BD", content = @Content(mediaType = "Application/Json")) })
	@PutMapping("/{id:[0-9]+}/services")
	@PreAuthorize("hasRole('PRIVE2')")
	public ResponseEntity<User> addService(@PathVariable Long id, @Valid @RequestParam Long serviceId) {
		User user = userService.addService(id, serviceId);
		return ResponseEntity.ok(user);
	}

	@Operation(summary = "modifie la liste de services auxquels appartient un utilisateur", tags = "utilisateur", responses = {
			@ApiResponse(responseCode = "200", description = "Succès de l'opération", content = @Content(mediaType = "Application/Json", array = @ArraySchema(schema = @Schema(implementation = User.class)))),
			@ApiResponse(responseCode = "404", description = "Erreur: User non trouvé dans la BD", content = @Content(mediaType = "Application/Json")) })
	@PutMapping("/{id:[0-9]+}/service")
	@PreAuthorize("hasRole('PRIVE2')")
	public ResponseEntity<?> editService(@PathVariable Long id, @Valid @RequestParam Long oldServiceId,
			@Valid @RequestParam Long newServiceId) {
		return userService.editService(id, oldServiceId, newServiceId);
	}

	@Operation(summary = "modifie le role d'un utilisateurs", tags = "utilisateur", responses = {
			@ApiResponse(responseCode = "200", description = "Succès de l'opération", content = @Content(mediaType = "Application/Json", array = @ArraySchema(schema = @Schema(implementation = User.class)))),
			@ApiResponse(responseCode = "404", description = "Erreur: User non trouvé dans la BD", content = @Content(mediaType = "Application/Json")) })
	@PutMapping("/{id:[0-9]+}/role")
	@PreAuthorize("hasRole('PRIVE2')")
	public ResponseEntity<User> editRole(@PathVariable Long id, @Valid @RequestParam Long roleId) {
		User user = userService.editRole(id, roleId);
		return ResponseEntity.ok(user);
	}

	@Operation(summary = "modifie le password d'un utilisateur", tags = "utilisateur", responses = {
			@ApiResponse(responseCode = "200", description = "Succès de l'opération", content = @Content(mediaType = "Application/Json")),
			@ApiResponse(responseCode = "400", description = "Erreur: Ancien mot de passe incorrect", content = @Content(mediaType = "Application/Json")) })
	@PreAuthorize("@authorizationService.canUpdatePersonalInformations('update', 'User', #id)")
	@PutMapping("/{id:[0-9]+}/password-update")
	public ResponseEntity<String> editPassword(@PathVariable Long id,
			@Valid @RequestBody UserEditPasswordDto userEditPasswordDto) {
		return userService.editPassword(id, userEditPasswordDto);
	}

	@Operation(summary = "reset le password d'un utilisateur", tags = "utilisateur", responses = {
			@ApiResponse(responseCode = "200", description = "Succès de l'opération", content = @Content(mediaType = "Application/Json")),
			@ApiResponse(responseCode = "404", description = "Erreur: User non trouvé dans la BD", content = @Content(mediaType = "Application/Json")) })
	@PutMapping("/{id:[0-9]+}/password-reset")
	@PreAuthorize("hasRole('PRIVE2')")
	public ResponseEntity<String> resetPassword(@PathVariable Long id) {
		userService.resetPassword(id);
		return ResponseEntity.ok("Mot de passe reinitialisé avec succès");
	}
}
