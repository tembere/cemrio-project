package cemrio.controller;

import java.util.List;
import java.util.stream.Collectors;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cemrio.dto.user.UserAuthDtoRequest;
import cemrio.dto.user.UserAuthDtoResponse;
import cemrio.security.jwt.JwtUtils;
import cemrio.security.service.UserDetailsImpl;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.ArraySchema;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.tags.Tag;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@Tag(name = "authentification")
@RequestMapping("/login")
public class AuthenticationRest {

	@Autowired
	AuthenticationManager authenticationManager;

	@Autowired
	JwtUtils jwtUtils;

	@Operation(summary = "Authentifie un utilisateur", tags = "authentification", responses = {
			@ApiResponse(responseCode = "200", description = "Succès de l'opération", content = @Content(mediaType = "Application/Json", array = @ArraySchema(schema = @Schema(implementation = UserAuthDtoResponse.class)))),
			@ApiResponse(responseCode = "400", description = "Bad Request", content = @Content(mediaType = "Application/Json")),
			@ApiResponse(responseCode = "401", description = "Bad credentials", content = @Content(mediaType = "Application/Json")) })
	@PostMapping
	public ResponseEntity<Object> authenticateUser(@Valid @RequestBody UserAuthDtoRequest userAuthDto) {
		Authentication authentication = authenticationManager
				.authenticate(
						new UsernamePasswordAuthenticationToken(userAuthDto.getUsername(), userAuthDto.getPassword()));
		SecurityContextHolder.getContext().setAuthentication(authentication);
		String jwt = jwtUtils.generateJwtToken(authentication);
		UserDetailsImpl userDetails = (UserDetailsImpl) authentication.getPrincipal();
		List<String> roles = userDetails.getAuthorities().stream() // @formatter:off
				.map(item -> item.getAuthority())
				.collect(Collectors.toList()); // @formatter:on
		return ResponseEntity.ok(
				new UserAuthDtoResponse(jwt, userDetails.getId(), userDetails.getUsername(), userDetails.getEmail(),
						roles));
	}
}
