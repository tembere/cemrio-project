package cemrio.entity;

import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import org.hibernate.envers.Audited;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonFormat.Shape;

import cemrio.audit.Auditable;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.media.Schema.AccessMode;
import lombok.AccessLevel;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Getter
@Setter
@NoArgsConstructor
@EqualsAndHashCode
@EntityListeners(AuditingEntityListener.class)
@Audited
public class Patient extends Auditable<String> {

	@Schema(description = "identifiant unique du patient", example = "1", required = true, accessMode = AccessMode.READ_ONLY)
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Setter(AccessLevel.NONE)
	private Long id;

	@Schema(description = "nom du patient", example = "Bouchua")
	@NotNull
	@Size(min = 1, max = 20)
	private String nom;

	@Schema(description = "prénom du patient", example = "Stéphane")
	@Size(min = 1, max = 100)
	private String prenom;

	@Schema(description = "age du patient", example = "17")
	@Column(nullable = true)
	private double age;

	@NotNull
	@Schema(description = "sexe du patient", example = "M")
	private char sexe;

	@Schema(description = "profession du patient", example = "Ingénieur Informatique", minLength = 1, maxLength = 80)
	@Size(min = 1, max = 80)
	private String profession;

	@Schema(description = "Téléphone du patient", example = "696225977")
	@Pattern(regexp = "^6(9|8|7|6|5)[0-9]{7}$")
	private String telephone;

	@Schema(description = "date des dernières règles du patient", example = "25-08-2020")
	@JsonFormat(shape = Shape.STRING, pattern = "dd-MM-yyyy")
	private LocalDate ddr;

	// @JsonFormat(pattern = "dd-MM-yyyy")
	// private LocalDateTime createdAt;

	// @ManyToMany
	// private Set<Examen> examens;

	// @ManyToMany
	// private Set<Prescripteur> prescripteurs;

	// @ManyToOne
	// private Prescripteur prescripteur;

	// @ManyToOne
	// private User user;

	// @JsonIgnore

	// @OneToMany(mappedBy = "patient") // , cascade = {
	// CascadeType.PERSIST,CascadeType.MERGE })
	// Set<PatientExamPresc> patientExamPrescs;

	@ManyToOne
	private User user;

	// @OneToMany(mappedBy = "patient")
	// private Set<EditPatient> editPatients = new HashSet<>();
}

/*
 * mettre le champ prescripteur en fetch lazy crée une erreur car le
 * champconstitue une colonne de la table patient ainsi jackson a du mal à le
 * serialiser : json - Aucun sérialiseur trouvé pour la classe
 * org.hibernate.proxy.pojo.javassist.Javassist? - Débordement de pile. Ce qui
 * n'est pas le cas dans le champ precedent car il ne constitue pas une colonne
 * de la table mais une table distincte. Meme problème avec le champ category
 * dans l'entité examen, et categoryParent dans l'entité Categorie
 */
