package cemrio.service;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import cemrio.dto.patient.PatientDtoExportReq;
import cemrio.entity.ERole;
import cemrio.entity.Patient;
import cemrio.entity.PatientExamPresc;
import cemrio.entity.Privilege;
import cemrio.entity.Role;
import cemrio.entity.User;
import cemrio.exception.UserNotFoundException;
import cemrio.repository.IPatientExamPrescRepo;
import cemrio.repository.IPatientRepo;
import cemrio.repository.IPrivilegeRepo;
import cemrio.repository.IRoleRepo;
import cemrio.repository.IUserRepo;
import cemrio.security.service.UserDetailsImpl;
import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class AuthorizationService {

	@Autowired
	private IPrivilegeRepo privilegeRepo;

	@Autowired
	private IUserRepo userRepo;

	@Autowired
	private IPatientRepo patientRepo;

	@Autowired
	private IPatientService patientService;

	@Autowired
	private IPatientExamPrescRepo patientExamPrescRepo;

	@Autowired
	private IPatientExamPrescService patientExamPrescService;

	@Autowired
	private IRoleRepo roleRepo;

	/**
	 * Vérifie l'autorisation pour les privileges qui ne sont pas sensé contenir de
	 * contraintes sur l'objet visé
	 */

	public boolean can(String action, String entity) {
		Privilege privilege = getPrivilege(action, entity);
		return (null != privilege && !privilege.isConstrained());
	}

	/**
	 * Vérifie l'autorisations pour les privileges qui comportent des contraintes
	 * sur l'objet visé
	 */

	public boolean canUpdatePersonalInformations(String action, String entity, Long entityId) {
		boolean authorized = false;
		Privilege privilege = getPrivilege(action, entity);
		if (null == privilege) {
			log.info("privilège non existant");
			return authorized;
		}
		User currentUser = getUser();
		switch (entity) {
		case "User": {
			if (currentUser.getId().equals(entityId)) {
				authorized = true;
			}
			break;
		}
		case "Patient":
		case "Prescripteur": {
			Role userRole = getGreaterRoleUser(currentUser);
			if (userRole.getNom().equals(ERole.ROLE_CLASSE1) || userRole.getNom().equals(ERole.ROLE_CLASSE3)) {
				List<Patient> patients = patientRepo.findAllByCreatedAtBetween(LocalDateTime.now().minusDays(7),
						LocalDateTime.now());
				Patient patient = patientService.get(entityId);
				if (patients.contains(patient)) {
					authorized = true;
				}
			}
			break;
		}

		default:
			break;
		}
		return authorized;
	}

	public boolean canUpdateAggregateInformations(String action, String entity, Long entityId) {
		boolean authorized = false;
		Privilege privilege = getPrivilege(action, entity);
		if (privilege != null) {
			User currentUser = getUser();
			if (entity.equals("PatientExamPresc")) {
				Role userRole = getGreaterRoleUser(currentUser);
				authorized = updateAndDeleteRules(userRole, entityId);
			}
		}
		return authorized;
	}

	public boolean canDelete(String action, String entity, Long entityId) {
		boolean authorized = false;
		Privilege privilege = getPrivilege(action, entity);
		if (null == privilege) {
			return authorized;
		}
		User currentUser = getUser();
		switch (entity) {
		case "Patient":
		case "Prescripteur":
		case "PatientExamPresc": {
			Role userRole = getGreaterRoleUser(currentUser);
			authorized = updateAndDeleteRules(userRole, entityId);
			break;
		}

		default:
			break;
		}
		return authorized;

	}

	public boolean canExtract(PatientDtoExportReq p) {
		boolean authorized = false;
		LocalDate from = p.getStartDate();
		LocalDate to = p.getEndDate();
		User currentUser = getUser();
		Role userRole = getGreaterRoleUser(currentUser);
		switch (userRole.getNom()) {
		case ROLE_CLASSE2: {
			if ((from.isEqual(LocalDate.now()) || from.isEqual(LocalDate.now().minusDays(1)))
					&& (to.isEqual(LocalDate.now().minusDays(1)) || to.isEqual(LocalDate.now()))) {
				authorized = true;
			}
			break;
		}
		case ROLE_CLASSE3:
		case ROLE_PRIVE1: {
			if (((from.isEqual(LocalDate.now().minusMonths(2)) || from.isAfter(LocalDate.now().minusMonths(2)))
					&& (from.isBefore(LocalDate.now()) || from.isEqual(LocalDate.now())))
					&& ((to.isEqual(LocalDate.now()) || to.isBefore(LocalDate.now()))
							&& (to.isAfter(LocalDate.now().minusMonths(2))
									|| to.isEqual(LocalDate.now().minusMonths(2))))) {
				authorized = true;
			}
			break;
		}
		case ROLE_PRIVE2: {
			authorized = true;
			break;
		}
		default:
			break;
		}
		return authorized;
	}

	public User getUser() {
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		if (authentication == null || !authentication.isAuthenticated()) {
			return null;
		}
		UserDetailsImpl currentUser = (UserDetailsImpl) authentication.getPrincipal();
		return userRepo.findById(currentUser.getId()).orElseThrow(() -> new UserNotFoundException(currentUser.getId()));
	}

	private Privilege getPrivilege(String action, String entity) {
		User currentUser = getUser();
		return privilegeRepo.findTopByActionAndEntityAndRolesInOrderByRolesDesc(action, entity,
				currentUser.getRoles());
	}

	public Role getGreaterRoleUser(User currentUser) {
		Set<Role> roles = currentUser.getRoles();
		Long idRoleMax = 1L;
		for (Role role : roles) {
			if (role.getId() > idRoleMax) {
				idRoleMax = role.getId();
			}
		}
		return roleRepo.findById(idRoleMax).orElseThrow(() -> new RuntimeException("Erreur: Role non trouvé."));
	}

	private boolean updateAndDeleteRules(Role userRole, Long entityId) {
		boolean authorised = false;
		if (userRole.getNom().equals(ERole.ROLE_CLASSE1) || userRole.getNom().equals(ERole.ROLE_CLASSE3)) {
			LocalDateTime from = LocalDate.now().atTime(8, 0, 0, 0);
			LocalDateTime to = LocalDate.now().atTime(17, 0, 0, 0);
			List<PatientExamPresc> patientExamPrescs = patientExamPrescRepo.findAllByCreatedAtBetween(from, to);
			PatientExamPresc patientExamPresc = patientExamPrescService.get(entityId);
			if (patientExamPrescs.contains(patientExamPresc)) {
				Set<cemrio.entity.Service> servicesUser = patientExamPresc.getUser().getServices();
				cemrio.entity.Service serviceExamen = patientExamPresc.getExamen().getCategorie().getService();
				if (servicesUser.contains(serviceExamen)) {
					authorised = true;
				}
			}
		}
		return authorised;
	}

	/*
	 * switch (action) { case "update": case "delete": // if (idRoleMax == 1 ||
	 * idRoleMax == 2) { if (1 == 1 || 2 == 2) { List<Patient> patients =
	 * patientRepo.findTop3ByUserOrderByIdDesc(user); if (!patients.isEmpty()) {
	 * Patient patient = patientRepo.findById(entityId).orElseThrow(() -> new
	 * PatientNotFoundException(entityId)); if (patients.contains(patient)) {
	 * authorized = true; } } break; } if (3 == 3) { List<Patient> patients =
	 * patientRepo.findByCreatedAtBetween(LocalDate.now().minusDays(2).atStartOfDay(
	 * ), LocalDate.now().plusDays(1).atStartOfDay()); if (!patients.isEmpty()) {
	 * Patient patient = patientRepo.findById(entityId).orElseThrow(() -> new
	 * PatientNotFoundException(entityId)); if (patients.contains(patient)) {
	 * authorized = true; } } } break; default: break; } return authorized; }/*
	 * public boolean canReadOrExport(String action, String entity, Long entityId) {
	 * 
	 * boolean authorized = false; Privilege privilege = getPrivilege(action,
	 * entity); if (null == privilege) { return authorized; } User user = getUser();
	 * 
	 * Set<Role> roles = user.getRoles(); Long idRoleMax = 1L; for (Role role :
	 * roles) { if (role.getId() > idRoleMax) { idRoleMax = role.getId(); } }
	 * 
	 * switch (action) { case "read": if (1 == 1) { List<Patient> patients =
	 * patientRepo.findTop3ByUserOrderByIdDesc(user); if (!patients.isEmpty()) {
	 * Patient patient = patientRepo.findById(entityId).orElseThrow(() -> new
	 * PatientNotFoundException(entityId)); if (patients.contains(patient)) {
	 * authorized = true; } } break; }
	 * 
	 * if (idRoleMax == 2) { List<Patient> patients =
	 * patientRepo.findByCreatedAt(LocalDateTime.now().as); if (!patients.isEmpty())
	 * { Patient patient = patientRepo.findById(entityId).orElseThrow(() -> new
	 * PatientNotFoundException(entityId)); if (patients.contains(patient)) {
	 * authorized = true; } } }
	 * 
	 * break; case "export": break; default: break; } return authorized; } }
	 * 
	 * // Vérification si l'utilisateur visé par la requete est le même que
	 * l'utilisateur actuellement // authentifié
	 * 
	 * case"User":if(currentUser.getId().equals(entityId)){authorized=true;}break;
	 */

}