package cemrio.service;

import java.util.List;

import cemrio.entity.Service;

public interface IServiceService {

	Service get(Long serviceId);

	List<Service> getAll();

}
