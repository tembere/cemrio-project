package cemrio.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import cemrio.entity.Prescripteur;

public interface IPrescripteurRepo extends JpaRepository<Prescripteur, Long> {
	public boolean existsByNomAndPrenom(String nom, String prenom);

	public boolean existsByEmailOrTelephone(String email, String telephone);

	public List<Prescripteur> findByNomIgnoreCase(String nom);

}
