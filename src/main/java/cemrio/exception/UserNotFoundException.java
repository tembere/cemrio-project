package cemrio.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@ResponseStatus(HttpStatus.NOT_FOUND)
public class UserNotFoundException extends RuntimeException {
	public UserNotFoundException(String message) {
		super(message);
		log.error("L'utilisateur " + message + " n'a pas été trouvé ");
	}

	public UserNotFoundException(Long id) {
		super("L'utilisateur de id " + id + " n'a pas été trouvé ");
		log.error("L'utilisateur de id " + id + " n'a pas été trouvé ");
	}
}
