package cemrio.service;

import java.util.List;

import javax.validation.Valid;

import org.springframework.data.domain.Page;

import cemrio.entity.Categorie;

public interface IcategorieService {

	Categorie add(Categorie cat);

	Categorie get(Long id);

	Page<Categorie> getAll();

	List<Categorie> getByNom(@Valid String nom);

	void delete(Long id);

	Categorie editName(Long id, @Valid String nom);

	Categorie editDescription(Long id, @Valid String description);

	Categorie editCategorieParent(Long id, @Valid Long catParentId);

	Categorie editService(Long id, @Valid Long serviceId);

	Categorie edit(Long id, Categorie categorie);

}
