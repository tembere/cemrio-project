package cemrio.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.Length;

import com.fasterxml.jackson.annotation.JsonIgnore;

import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.media.Schema.AccessMode;
import lombok.AccessLevel;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Getter
@Setter
@NoArgsConstructor
@EqualsAndHashCode
public class Categorie {

	@Schema(description = "identifiant unique de la catégorie de l'examen", example = "1", required = true, accessMode = AccessMode.READ_ONLY)
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Setter(AccessLevel.NONE)
	private Long id;

	@Schema(description = "nom de la catégorie de l'examen/nom de la catégorie parente", example = "Radiologie Standard")
	@NotNull
	@Length(min = 1, max = 50)
	private String nom;

	@Schema(description = "description de la catégorie", example = "description")
	@Size(min = 1, max = 200)
	private String description;

	@ManyToOne
	@JoinColumn(name = "parent_id")
	private Categorie categoryParent;

	@ManyToOne
	@JsonIgnore
	private User user;

	@ManyToOne
	@NotNull
	private Service service;
}
