package cemrio.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.validation.constraints.NotNull;

import org.hibernate.envers.Audited;

import cemrio.audit.Auditable;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AccessLevel;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Getter
@Setter
@NoArgsConstructor
@EqualsAndHashCode
@Audited
public class PatientExamPresc extends Auditable<String> {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Setter(AccessLevel.NONE)
	private Long id;

	@ManyToOne // (cascade = { CascadeType.PERSIST, CascadeType.MERGE })
	@JoinColumn(name = "patient_id")
	Patient patient;

	@ManyToOne // (cascade = { CascadeType.PERSIST, CascadeType.MERGE })
	@JoinColumn(name = "examen_id")
	Examen examen;

	@ManyToOne // (cascade = { CascadeType.PERSIST, CascadeType.MERGE })
	@JoinColumn(name = "prescripteur_id")
	Prescripteur prescripteur;

	@Schema(description = "renseignements cliniques du patient", example = "raisons de l'examen")
	@NotNull
	private String renseignementsCliniques;

	// @JsonIgnore
	@ManyToOne
	private User user;
}
