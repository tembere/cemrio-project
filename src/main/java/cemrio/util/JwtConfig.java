package cemrio.util;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

import lombok.Getter;

@Getter
@Component
public class JwtConfig {
	@Value("${security.jwt.uri:/login/**}")
	private String uri;

	@Value("${security.jwt.header:Authorization}")
	private String header;

	@Value("${security.jwt.prefix:Bearer }")
	private String prefix;

	@Value("${security.jwt.expiration:#{24*60*60}}")
	private int expiration;

	@Value("${security.jwt.secret:cemrioSecretKey}")
	private String secret;

	@Bean
	public ModelMapper modelMapper() {
		return new ModelMapper();
	}
}
