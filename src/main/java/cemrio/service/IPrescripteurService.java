package cemrio.service;

import java.util.List;

import javax.validation.Valid;

import org.springframework.data.domain.Page;
import org.springframework.http.ResponseEntity;

import cemrio.entity.Prescripteur;

public interface IPrescripteurService {
	public ResponseEntity<?> add(Prescripteur prescripteur);

	public Prescripteur get(Long id);

	public Page<Prescripteur> getAll();

	public Prescripteur edit(Long id, Prescripteur prescripteur);

	public void delete(Long id);

	public List<Prescripteur> getByNom(@Valid String nom);

	public Prescripteur editName(Long id, @Valid String nom);

	public Prescripteur editSurname(Long id, @Valid String prenom);

	public Prescripteur editTelephone(Long id, @Valid String telephone);

	public Prescripteur editEmail(Long id, @Valid String email);
}
