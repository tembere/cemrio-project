package cemrio.dto.patientExamPresc;

import javax.validation.constraints.NotNull;

import cemrio.entity.Examen;
import cemrio.entity.Patient;
import cemrio.entity.Prescripteur;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class PatientExamPrescDto {

	@NotNull
	Patient patient;
	@NotNull
	Examen examen;
	Prescripteur prescripteur;
}
