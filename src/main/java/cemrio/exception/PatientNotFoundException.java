package cemrio.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.NOT_FOUND)
public class PatientNotFoundException extends RuntimeException {

	public PatientNotFoundException(String message) {
		super(message);
	}

	public PatientNotFoundException(Long id) {
		super("Le Patient de id " + id + " n'a pas été trouvé  ");
	}

}
