package cemrio.service;

import java.util.List;
import java.util.Set;

import javax.validation.Valid;

import org.springframework.data.domain.Page;

import cemrio.dto.patient.PatientDtoExportReq;
import cemrio.entity.Patient;
import cemrio.entity.PatientExamPresc;

public interface IPatientExamPrescService {

	Set<PatientExamPresc> add(Set<PatientExamPresc> p);

	Set<PatientExamPresc> get(Patient p);

	PatientExamPresc get(Long id);

	Page<PatientExamPresc> getAllPatientsWithExamensAndPrescripteurs();

	void delete(Long id);

	PatientExamPresc editRenseignementsCligniques(Long id, @Valid String nom);

	PatientExamPresc editPrescripteur(Long id, Long prescripteurId);

	PatientExamPresc editExamen(Long id, Long examenId);

	PatientExamPresc editPatient(Long id, Long patientId);

	PatientExamPresc edit(Long id, PatientExamPresc p);

	List<PatientExamPresc> export(PatientDtoExportReq p);

}
