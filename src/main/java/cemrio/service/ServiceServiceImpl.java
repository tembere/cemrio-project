package cemrio.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import cemrio.entity.Service;
import cemrio.repository.IserviceRepo;

@org.springframework.stereotype.Service
public class ServiceServiceImpl implements IServiceService {

	@Autowired
	private IserviceRepo serviceRepo;

	@Override
	public Service get(Long serviceId) {
		return serviceRepo.findById(serviceId)
				.orElseThrow(() -> new RuntimeException("Service de id " + serviceId + " non trouvé"));
	}

	@Override
	public List<Service> getAll() {
		return serviceRepo.findAll();
	}

}
