package cemrio.repository;

import java.time.LocalDateTime;
import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;

import cemrio.entity.Patient;
import cemrio.entity.User;

public interface IPatientRepo extends JpaRepository<Patient, Long> {
	List<Patient> findByNom(String nom);

	Page<Patient> findAllByCreatedAtBetween(@Param("FROM") LocalDateTime from, @Param("to") LocalDateTime to,
			Pageable pageable);

	List<Patient> findAllByCreatedAtBetween(@Param("FROM") LocalDateTime from, @Param("to") LocalDateTime to);

	Page<Patient> findAllByCreatedAtBetweenAndUser(@Param("FROM") LocalDateTime from, @Param("to") LocalDateTime to,
			User user, Pageable pageable);

	List<Patient> findAllByCreatedAt(LocalDateTime now);

}
