package cemrio.dto.categorie;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.Length;

import cemrio.entity.Categorie;
import cemrio.entity.Service;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CategorieDtoReq {

	@NotNull
	@Length(min = 1, max = 50)
	private String nom;

	@Size(min = 1, max = 200)
	private String description;

	private Categorie categoryParent;

	@NotNull
	private Service service;
}
