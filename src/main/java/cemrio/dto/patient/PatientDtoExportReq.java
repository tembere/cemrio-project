
package cemrio.dto.patient;

import java.time.LocalDate;
import java.util.Set;

import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonFormat;

import cemrio.entity.Examen;
import cemrio.entity.Prescripteur;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class PatientDtoExportReq {
	private Set<Prescripteur> prescripteurs;
	private Set<Examen> examens;

	@NotNull
	@JsonFormat(pattern = "dd-MM-yyyy")
	private LocalDate startDate;

	@NotNull
	@JsonFormat(pattern = "dd-MM-yyyy")
	private LocalDate endDate;
}
