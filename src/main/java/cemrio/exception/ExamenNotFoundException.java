package cemrio.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.NOT_FOUND)
public class ExamenNotFoundException extends RuntimeException {
	public ExamenNotFoundException(String message) {
		super(message);
	}

	public ExamenNotFoundException(Long id) {
		super("L'examen de id " + id + " n'a pas été trouvé  ");
	}
}
