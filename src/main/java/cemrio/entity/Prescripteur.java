package cemrio.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import org.hibernate.envers.Audited;

import com.fasterxml.jackson.annotation.JsonIgnore;

import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.media.Schema.AccessMode;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Getter
@Setter
@NoArgsConstructor
@Audited
public class Prescripteur {

	@Schema(description = "identifiant unique du prescripteur", example = "1", required = true, accessMode = AccessMode.READ_ONLY)
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Setter(AccessLevel.NONE)
	private Long id;

	@Schema(description = "nom du prescripteur", example = "Dr Mohamadou")
	@NotNull
	@Size(min = 1, max = 100)
	private String nom;

	@Schema(description = "prénom du prescripteur", example = " bachirou")
	@NotNull
	@Size(min = 1, max = 100)
	private String prenom;

	@Schema(description = "email du prescripteur", example = "stephane@gmail.com")
	@Column(unique = true)
	@Email
	@Size(max = 50)
	private String email;

	@Schema(description = "Téléphone du patient", example = "696225977")
	@Pattern(regexp = "^6(9|8|7|6|5)[0-9]{7}$")
	private String telephone;

	@ManyToOne
	@JsonIgnore
	private User user;

	/*
	 * @JsonIgnore
	 * 
	 * @OneToMany(mappedBy = "prescripteur", cascade = { CascadeType.PERSIST,
	 * CascadeType.MERGE }) Set<PatientExamPresc> patientExamPrescs;
	 */
}
