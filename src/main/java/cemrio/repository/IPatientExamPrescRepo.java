package cemrio.repository;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Set;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.jpa.repository.JpaRepository;

import cemrio.entity.Examen;
import cemrio.entity.Patient;
import cemrio.entity.PatientExamPresc;
import cemrio.entity.Prescripteur;

public interface IPatientExamPrescRepo extends JpaRepository<PatientExamPresc, Long> {

	List<PatientExamPresc> findAllByCreatedAtBetween(LocalDateTime from, LocalDateTime to);

	Page<PatientExamPresc> findAllByCreatedAtBetween(LocalDateTime from, LocalDateTime to, PageRequest of);

	Set<PatientExamPresc> findByPatient(Patient p);

	List<PatientExamPresc> findByPrescripteurIn(Set<Prescripteur> p);

	List<PatientExamPresc> findByExamenIn(Set<Examen> e);
}
