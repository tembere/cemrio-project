package cemrio.service;

import java.util.List;

import javax.validation.Valid;

import org.springframework.data.domain.Page;

import cemrio.entity.Examen;

public interface IExamenService {

	public Examen get(Long id);

	public Page<Examen> getAll();

	public Examen add(Examen e);

	public void delete(Long id);

	public Examen editName(Long id, @Valid String nom);

	public Examen editDescription(Long id, @Valid String description);

	public Examen editCategory(Long id, Long categorieId);

	public Examen edit(Long id, Examen exam);

	public List<Examen> getByNom(String nom);
}
