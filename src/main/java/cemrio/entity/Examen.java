package cemrio.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.hibernate.envers.Audited;
import org.hibernate.envers.NotAudited;

import com.fasterxml.jackson.annotation.JsonIgnore;

import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.media.Schema.AccessMode;
import lombok.AccessLevel;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Getter
@Setter
@NoArgsConstructor
@EqualsAndHashCode
@Audited
public class Examen {

	@Schema(description = "identifiant unique de l'examen", example = "1", required = true, accessMode = AccessMode.READ_ONLY)
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Setter(AccessLevel.NONE)
	private Long id;

	@Schema(description = "nom de l'examen", example = "Thorax")
	@NotNull
	@Size(min = 1, max = 50)
	private String nom;

	@Schema(description = "description de l'examen", example = "examen disponible en vue de face et en vue de profil")
	@Size(min = 1, max = 200)
	private String description;

	@ManyToOne
	@NotAudited
	@JsonIgnore
	private Categorie categorie;

	@JsonIgnore
	@ManyToOne
	private User user;

	/*
	 * @JsonIgnore
	 * 
	 * @OneToMany(mappedBy = "examen", cascade = { CascadeType.PERSIST,
	 * CascadeType.MERGE }) private Set<PatientExamPresc> patientExamPrescs;
	 */

}
