package cemrio.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import cemrio.entity.Examen;

public interface IExamenRepo extends JpaRepository<Examen, Long> {

	List<Examen> findByNomIgnoreCase(String nom);

}
