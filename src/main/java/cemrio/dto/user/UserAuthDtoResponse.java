
package cemrio.dto.user;

import java.util.List;

import lombok.Getter;
import lombok.Setter;

@Getter

@Setter
public class UserAuthDtoResponse {
	private String token;
	private String type = "Bearer";
	private Long id;
	private String username;
	private String email;
	private List<String> roles;

	public UserAuthDtoResponse(String accessToken, Long id, String username, String email, List<String> roles) {
		this.token = accessToken;
		this.id = id;
		this.username = username;
		this.email = email;
		this.roles = roles;
	}
}
