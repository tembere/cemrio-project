
package cemrio.dto.patient;

import java.time.LocalDateTime;

import com.fasterxml.jackson.annotation.JsonFormat;

import cemrio.entity.Examen;
import cemrio.entity.Patient;
import cemrio.entity.Prescripteur;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class PatientDtoExportRes {

	@JsonFormat(pattern = "dd-MM-yyyy HH:mm")
	private LocalDateTime createdAt;
	private Patient patient;
	private Prescripteur prescripteur;
	private Examen examens;
}
