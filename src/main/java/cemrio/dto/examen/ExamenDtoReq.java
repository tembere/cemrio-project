package cemrio.dto.examen;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import cemrio.entity.Categorie;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ExamenDtoReq {

	@NotNull
	@Size(min = 1, max = 50)
	private String nom;

	@Size(min = 1, max = 200)
	private String description;

	private Categorie categorie;
}
