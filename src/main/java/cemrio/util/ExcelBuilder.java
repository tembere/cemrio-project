
package cemrio.util;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.time.ZoneId;
import java.util.Date;
import java.util.List;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.DateUtil;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import cemrio.dto.patient.PatientDtoExportRes;

public class ExcelBuilder {

	private ExcelBuilder() {
		throw new IllegalStateException("Utility class");
	}

	public static ByteArrayInputStream patientsToExcel(List<PatientDtoExportRes> pDtoExpres) throws IOException {
		String[] columns = { "Date", "Prescripteur", "Patient", "Examen" };
		try (Workbook workbook = new XSSFWorkbook(); ByteArrayOutputStream out = new ByteArrayOutputStream();) {

			Sheet sheet = workbook.createSheet("patients");

			Font headerFont = workbook.createFont();
			headerFont.setBold(true);
			headerFont.setColor(IndexedColors.BLUE.getIndex());

			CellStyle headerCellStyle = workbook.createCellStyle();
			headerCellStyle.setFont(headerFont);

			// Row for Header
			Row headerRow = sheet.createRow(0);

			// Header
			for (int col = 0; col < columns.length; col++) {
				Cell cell = headerRow.createCell(col);
				cell.setCellValue(columns[col]);
				cell.setCellStyle(headerCellStyle);
			}

			int rowIdx = 1;
			for (PatientDtoExportRes patient : pDtoExpres) {
				Date date = Date.from(patient.getCreatedAt().atZone(ZoneId.systemDefault()).toInstant());
				Row row = sheet.createRow(rowIdx++);
				row.createCell(0).setCellValue(DateUtil.getExcelDate(date));
				row.createCell(1).setCellValue(patient.getPrescripteur().getNom() + " " +
						patient.getPrescripteur().getPrenom());
				row.createCell(2).setCellValue(patient.getPatient().getNom() + " " +
						patient.getPatient().getPrenom());
				row.createCell(3).setCellValue(patient.getExamens().toString());
				/*
				 * row.createCell(3).setCellValue(patient.getExamens().stream().map(examen ->
				 * examen.getNom()) .collect(Collectors.toList()).toString());
				 */

			}

			workbook.write(out);
			return new ByteArrayInputStream(out.toByteArray());
		}
	}
}
